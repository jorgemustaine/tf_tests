# Create basic GKE backup Plan
resource "google_gke_backup_backup_plan" "basic" {
  name = "basic-plan-jler"
  cluster = google_container_cluster.primary.id
  location = var.region
  labels = var.cluster_resource_labels
  backup_config {
    include_volume_data = true
    include_secrets = true
    all_namespaces = true
  }

}


# Create full GKE backup Plan
resource "google_gke_backup_backup_plan" "full" {
  name = "full-plan-jler"
  cluster = google_container_cluster.primary.id
  location = var.region
  labels = var.cluster_resource_labels
  backup_config {
    include_volume_data = true
    include_secrets = true
    all_namespaces = true
  }
  retention_policy {
    backup_delete_lock_days = 30
    backup_retain_days = 180
  }
  backup_schedule {
    cron_schedule = "10 3 * * *"
    # next label permit us to stop schedule jobs
    # paused = True
  }

}


# Create GKE backup only namespaces (workloads)
resource "google_gke_backup_backup_plan" "namespaces_bck" {
  name = "namespaces-plan-jler"
  cluster = google_container_cluster.primary.id
  # Here I'm trying to configure a diff region for our backup
  location = var.region_bck
  labels = var.cluster_resource_labels
  backup_config {
    include_volume_data = false
    include_secrets = false
    all_namespaces = true
  }
  retention_policy {
    backup_delete_lock_days = 30
    backup_retain_days = 180
  }
  backup_schedule {
    cron_schedule = "5 4 * * *"
    # next label permit us to stop schedule jobs
    paused = true
  }
}


# Create GKE backup only secrets (keys, apikeys, passwords, etc)
# It's so important to deploy another cluster components for this use case
# Named ProtectedApplication in that micro manifest you should specify which apps
# do you want to backup and How. you can look around in:
# https://cloud.google.com/kubernetes-engine/docs/add-on/backup-for-gke/how-to/protected-application
# We made an example in protected_application.yaml
resource "google_gke_backup_backup_plan" "secrets_bck" {
  name = "secrets-plan-jler"
  cluster = google_container_cluster.primary.id
  # Here I'm trying to configure a diff region for our backup
  location = var.region_bck
  backup_config {
    include_volume_data = false
    include_secrets = true
    # all_namespaces = true
    selected_applications {
      namespaced_names {
        name = var.workload_app_1
        namespace = "default"
      }
      # Here we can add other app's components
      # namespaced_names {
      #   name = var.workload_app_2
      #   namespace = "default"
      # }
    }
  }
  retention_policy {
    backup_delete_lock_days = 30
    backup_retain_days = 180
  }
  backup_schedule {
    cron_schedule = "5 5 * * *"
   # next label permit us to stop schedule jobs
   paused = true
  }
}


# Create GKE backup only volume data (persistent disks, configmaps)
# resource "google_gke_backup_backup_plan" "volume_bck" {
#   name = "volume-plan-jler"
#   cluster = google_container_cluster.primary.id
#   # Here I'm trying to configure a diff region for our backup
#   location = var.region_bck
#   backup_config {
#     include_volume_data = true
#     include_secrets = false
#     all_namespaces = false
#   }
#   retention_policy {
#     backup_delete_lock_days = 30
#     backup_retain_days = 180
#   }
#   backup_schedule {
#     cron_schedule = "5 3 * * *"
#     # next label permit us to stop schedule jobs
#     # paused = True
#   }
# }