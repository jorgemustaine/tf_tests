# GKE cluster
resource "google_container_cluster" "primary" {
  name     = "${var.project_id}-gke-jler"
  location = var.region
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1
  network         = var.vpc_network
  subnetwork      = var.sub_network
  resource_labels = var.cluster_resource_labels
  # activate gke backup agent
  addons_config {
    gke_backup_agent_config {
      enabled = true
    }
  }
}


# Declare node pool
resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${var.project_id}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = 1
  node_config {
    preemptible  = true
    machine_type = "e2-medium"

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    # service_account = google_service_account.default.email
    # service_account = "service_account = google_service_account.default.email"
    oauth_scopes    = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

  }
  autoscaling {
    # Minimum number of nodes in the NodePool. Must be >=0 and <= max_node_count.
    # min_node_count = lookup(var.node_pools[count.index], "autoscaling_min_node_count", 2)
    min_node_count = 2
    # Maximum number of nodes in the NodePool. Must be >= min_node_count.
    # max_node_count = lookup(var.node_pools[count.index], "autoscaling_max_node_count", 3)
    max_node_count = 3
  }
}

