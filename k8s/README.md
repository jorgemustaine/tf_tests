# quickstart tf on gcp


It's a simple tf momdule structure to deploy k8s on gcp.

## GKE backup

checking gke cluster backup is tur on with cli:

```
gcloud beta container clusters describe cluster-name \
--project=project_id  \
--zone=us-east1 | grep -A 1 gkeBackupAgentConfig:
```

**list all backups plans:**

```
gcloud beta container backup-restore backup-plans list \
--project=$PROJECT_ID \
--location=$REGION
```

**View the backups:**

```
gcloud beta container backup-restore backups list \
--project=$PROJECT_ID \
--location=$REGION \
--backup-plan=$BACKUP_PLAN
```

**View the details of a backup plan:**

```
gcloud beta container backup-restore backup-plans describe $BACKUP_PLAN \
--project=$PROJECT_ID \
--location=$REGION
```
**conecting to cluster:**

`gcloud container clusters get-credentials $(terraform output -raw kubernetes_cluster_name) --region $(terraform output -raw region)`

after you can try:

'kubectl get pods'

or another one.

## Template for ful GKE backup config:

```
resource "google_gke_backup_backup_plan" "full" {
  name = "full-plan"
  cluster = google_container_cluster.primary.id
  location = "us-central1"
  retention_policy {
    backup_delete_lock_days = 30
    backup_retain_days = 180
  }
  backup_schedule {
    cron_schedule = "0 9 * * 1"
  }
  backup_config {
    include_volume_data = true
    include_secrets = true
    selected_applications {
      namespaced_names {
        name = "app1"
        namespace = "ns1"
      }
      namespaced_names {
        name = "app2"
        namespace = "ns2"
      }
    }
  }
}
```

### interesting links:


* [GKE backup plan](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/gke_backup_backup_plan "GKE backup by hashicorp")
* [GKE backup API Documentation](https://cloud.google.com/kubernetes-engine/docs/add-on/backup-for-gke/reference/rest/v1/projects.locations.backupPlans "google api documentation")
* [ProtectedApplication](https://cloud.google.com/kubernetes-engine/docs/add-on/backup-for-gke/how-to/protected-application "How works GKE backup")
* [tf GKE](https://developer.hashicorp.com/terraform/tutorials/kubernetes/gke "Tutorial k8s GKE")
* [tf gcp refrence](https://registry.terraform.io/providers/hashicorp/google/latest/docs "terraform documentation about gcp")
* [Using GKE with tf](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/using_gke_with_terraform "Using GKE with tf")
* [GKE backup](https://gitlab.com/jorgemustaine/tech_courses/-/blob/master/gcp/qwiklabs/gke_backup_56619.md "GKE Backup")
* [qwiklab gke backup](https://www.cloudskillsboost.google/focuses/56619)


@jorgemustaine 2023
