# Define the necessary variables: The restore process will need to know some
# information about the backup that is being restored, such as the name of the
# backup and the GCP project and location where it was stored. You can define
# these as Terraform variables in a variables.tf file.
variable "project_id" {
  type = string
}

variable "location" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "node_pool_name" {
  type = string
}

variable "node_pool_size" {
  type = number
}

variable "node_pool_machine_type" {
  type = string
}

variable "backup_name" {
  type = string
}


# Create a google_container_cluster resource: This resource will represent
# the GKE cluster that you want to restore the backup to. You will need to
# provide the name of the cluster, as well as other configuration options, such
# as the number of nodes and machine type.
resource "google_container_cluster" "restored_cluster" {
  name               = var.cluster_name
  initial_node_count = var.node_pool_size
  node_config {
    machine_type = var.node_pool_machine_type
  }
  enable_binary_authorization = true
}

# Create a google_container_node_pool resource: This resource will represent
# the node pool in the GKE cluster. You will need to provide the name of the node
# pool, as well as other configuration options, such as the number of nodes and
# machine type.
resource "google_container_node_pool" "existing_nodes" {
  name       = var.node_pool_name
  node_count = var.node_pool_size
  node_config {
    machine_type = var.node_pool_machine_type
  }
}

resource "google_container_node_pool" "restored_nodes" {
  name       = "restored-pool"
  node_count = var.node_pool_size
  node_config {
    machine_type = var.node_pool_machine_type
  }
}

# Describe the backup to restore
resource "google_container_cluster_backup" "backup" {
  name     = var.backup_name
  location = var.location
}

# Create a restore operation for the backup: You can use the
# google_container_cluster_restore resource to create a restore operation for the
# backup. You will need to provide the name of the restore operation, the name
# of the backup, and the name of the node pool that the backup should be
# restored to.

resource "google_container_cluster_restore" "restore_operation" {
  restore_name = "restore-from-backup"
