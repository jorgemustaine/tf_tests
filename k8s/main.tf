##################################################################################
# VARIABLES
##################################################################################

variable "gcp_json_access_key" {}
variable "region" {}
variable "region_bck" {}
variable "zone" {}
variable "project_id" {}
variable "vpc_network" {}
variable "sub_network" {}
variable "cluster_resource_labels" {}
variable "service_account_id" {}
variable "workload_app_1" {}
variable "workload_app_2" {}
##################################################################################

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file(var.gcp_json_access_key)

  project = var.project_id
  region  = var.region
  zone    = var.zone
}

