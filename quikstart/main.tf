##################################################################################
# VARIABLES
##################################################################################

variable "gcp_json_access_key" {}
variable "region" {}
variable "zone" {}
variable "project_id" {}

##################################################################################

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "google" {
  credentials = file(var.gcp_json_access_key)

  project = var.project_id
  region  = var.region
  zone    = var.zone
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network-jorgescalona-test"
}

